#NEAR-BY

### What is this repository for? ###

* Quick summary
    -**NEAR-BY** is an application to show the near by Atms, Banks, Airports, Bus stops, Train stations and more.This is based on ReactJs, Redux, Redux-thunk as middleware and uses google api places near by for getting the user search results.Application prompts for an location access to show near by requirement and also provides the custom search option for searching your required place.Radius of 1km is initalised and can edit the radius near by from store object.Used Bootstrap for styling

* Version : 0.1.0

### How do I get set up? ###
* Required before clone
    - This repository imagines node, git are installed in his/her machine.

* Summary of set up
    - clone the repository from your terminal.
    - Use Git command git clone https://venkatsrikanth@bitbucket.org/venkatsrikanth/places-near-me.git.
    - Enter Near-by folder with cmd if windows : **cd near-by** / mac : **open near-by**
    - **npm install** (this will install all the dependencies required)
    - **npm start** (this will start your application on 3000 port on local folder) 

* Dependencies
    **axios, react, react-bootstrap, react-dom, react-redux, react-scripts, redux, redux-logger, redux-thunk**
    
### Folder structure and use

    .
    ├── public   
    │   ├── index.html  
    │   └── mainfest.json
    ├── src                    
    │   ├── actions         
    │   ├── components        
    │   ├── reducers      
    │   ├── App.js   
    │   └── Index.js
    ├── .gitignore             
    ├── package.json 
    └── README.md


### Who do I talk to? ###

* Repo owner or admin
    - Venkat Srikanth Pingali on Bit bucket / Hip chat
    - Mail id : venkat.ieee55@gmail.com
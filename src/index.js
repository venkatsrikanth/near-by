import React from 'react';
import ReactDOM from 'react-dom';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';


import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducer from './reducers';
import { fetchResults } from './actions';


const logger = createLogger({});
const middleWare = applyMiddleware(thunk, logger);
const store = createStore(reducer, middleWare);
const selectedType = store.getState().selectedType;
const currentLocation = store.getState().currentLocation;
const radius = store.getState().radius;

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

store.dispatch(fetchResults(selectedType, currentLocation, radius));
registerServiceWorker();



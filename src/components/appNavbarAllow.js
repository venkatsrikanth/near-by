import React, { Component } from 'react';
import {Navbar, NavDropdown, MenuItem, Nav, FormControl, Jumbotron, Grid, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { fetchResults, fetchFromCenter } from '../actions';

const mapStateToProps = function(state){
  return{
    searchTypes : state.types,
    selectedType : state.selectedType,
    access : state.userLocationAccess,
    currentLocation : state.currentLocation,
    radius : state.radius
  }
}


class AppNavbarAllow extends Component {
  constructor(props){
    super(props);
    this.changeType = this.changeType.bind(this);
    this.inputChanged = this.inputChanged.bind(this);
  }

  changeType(selectedType){
    if(selectedType.type !== this.props.selectedType.type){
      this.props.dispatch(fetchResults(selectedType, this.props.currentLocation, this.props.radius));
    }
  }

  inputChanged(e){
    var store = this.props;
    var selectedType = this.props.selectedType;
    var radius = this.props.radius;
    if(e.target.value){
        var searchBox = new window.google.maps.places.SearchBox(document.getElementById('input-box-block'));
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();
            if(places.length>0){
                var center = {lat:places[0].geometry.location.lat(), lng:places[0].geometry.location.lng()};
                store.dispatch({type:'SET_ADDRESS', payload:places[0].formatted_address})
                store.dispatch({type:'SET_CENTER_LOCATION',payload:center})
                store.dispatch(fetchFromCenter(center, selectedType, radius));
            }
        });
    }
  }

  render() {
    if(this.props.currentLocation){
      return(
        <Navbar collapseOnSelect fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <a>Near by</a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>     
            <NavDropdown title={this.props.selectedType.name} id="basic-nav-dropdown">
              {this.props.searchTypes.map((result, index)=>{
                return(
                  <MenuItem eventKey={result} key={index} onSelect={this.changeType}>{result.name}</MenuItem>
                )
              })}
            </NavDropdown>           
          </Nav>
         
        </Navbar.Collapse>
      </Navbar>
      )
    }else{
      return (
        <Grid>
          <Row className="show-grid">
            <Jumbotron className="top70">
              <h1> Near by </h1>
              <p>
                <img src="https://cdn0.iconfinder.com/data/icons/flat-vector-1/100/26-Pay_Now-512.png" className="autoWidth" alt=""></img>
                <img src="http://www.iconarchive.com/download/i87026/graphicloads/colorful-long-shadow/Bank.ico" className="autoWidth" alt=""></img>
                <img src="https://www.shareicon.net/download/2016/07/12/795057_medical_512x512.png" className="autoWidth" alt=""></img>
                <img src="https://cdn0.iconfinder.com/data/icons/medicine-and-medical-equipment/512/blood_bag_donation_medical_donate_transfusion_container_donor_human_flat_design_icon-512.png" className="autoWidth" alt=""></img>
                <img src="https://www.shareicon.net/download/2016/08/18/809778_airport_512x512.png" className="autoWidth" alt=""></img>
              </p>
              <p>
                Find near by Banks, Atms, Pharmacy's, Hospitals, Airports and more.
              </p>
              <p>
              <FormControl type="text" placeholder="Search place" id="input-box-block" onChange={this.inputChanged} autoFocus/>
            </p>
          </Jumbotron>
          </Row>
        </Grid>
        
      )
    }
   
  }
}

export default connect(mapStateToProps)(AppNavbarAllow);
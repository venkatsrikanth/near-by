import React, { Component } from 'react';
import { Col } from 'react-bootstrap';
import { connect } from 'react-redux';

const mapStateToProps = (state)=>{
    return{
        center: state.currentLocation,
        results: state.data,
        loading : state.isFetching,
        currentAddress : state.currentAddress
    }
}

class Map extends Component {
    constructor(props){
        super(props);
        this.map = {};
        this.createMarker = this.createMarker.bind(this);
        this.hideAllInfoWindows = this.hideAllInfoWindows.bind(this);
        this.markers = [];
    }
    initMap(){
        if(this.props.center && this.props.results.length>0 && Array.isArray(this.props.results)){
            this.map = new window.google.maps.Map(document.getElementById('map'), {
                center: this.props.center,
                zoom: 14
            });
            var currentIcon = {
                url: "https://cdn.iconscout.com/public/images/icon/premium/png-512/pin-location-pointer-marker-place-3f029c65a9b13cfe-512x512.png", // url
                scaledSize: new window.google.maps.Size(50, 50), // scaled size
                origin: new window.google.maps.Point(0,0), // origin
                anchor: new window.google.maps.Point(0, 0) // anchor
            },
            locationInfowindow = new window.google.maps.InfoWindow(),
            marker = new window.google.maps.Marker({
              map: this.map,
              position: this.props.center,
              icon: currentIcon,
              infowindow: locationInfowindow
            }),
            
            hideAllInfoWindows = this.hideAllInfoWindows,
            currentAddress = this.props.currentAddress;

            
            
            window.google.maps.event.addListener(marker, 'click', function () {
                hideAllInfoWindows(this.map);
                this.infowindow.setContent("<h5>"+ currentAddress +"</h5>");
                this.infowindow.open(this.map, this);
            });
            this.markers.push(marker);
    
            if(this.props.results.length>0){
                for (var i = 0; i < this.props.results.length; i++) {
                    this.createMarker(this.props.results[i]);
                }
            }
        }
    }

    hideAllInfoWindows(map) {
        this.markers.forEach(function(marker) {
          marker.infowindow.close(map, marker);
        }); 
    }

    createMarker(place) {
        var placeMarkers = {
                url: "https://cdn4.iconfinder.com/data/icons/peppyicons/512/660011-location-512.png", // url
                scaledSize: new window.google.maps.Size(40, 40), // scaled size
                origin: new window.google.maps.Point(0,0), // origin
                anchor: new window.google.maps.Point(0, 0) // anchor
            },
            locationInfowindow = new window.google.maps.InfoWindow(),
            marker = new window.google.maps.Marker({
                map: this.map,
                position: place.geometry.location,
                icon : placeMarkers,
                infowindow: locationInfowindow
            }),
            hideAllInfoWindows = this.hideAllInfoWindows;

            
            window.google.maps.event.addListener(marker, 'click', function () {
                hideAllInfoWindows(this.map);
                this.infowindow.setContent("<h5>" + place.name + "</h5><a></a>");
                this.infowindow.open(this.map, this);
            });
            this.markers.push(marker);
      }
      
    render(){
        this.initMap();
        if(this.props.results === "ZERO_RESULTS" || this.props.loading){
            return(
                <Col xs={12} md={6} className="hideMapOnMobile hidden">
                    <div id="map"></div>
                </Col>
            )
        }else if(this.props.center){
            return(
                <Col xs={12} md={6} className="hideMapOnMobile">
                  <div id="map"></div>
                </Col>
            )
        }else{
            return(
                <Col xs={12} md={6} className="hideMapOnMobile">
                  {/* <div id="map"></div> */}
                </Col>
            )
        }        
    }
}
export default connect(mapStateToProps)(Map);
import React, { Component } from 'react';
import { Col, Media, Label, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import giphy1 from '../giphy1.gif';
import empty_result from '../empty_result.png';
import { fetchFromCenter } from '../actions';
const mapStateToProps = (state)=>{
  return{
    loading:state.isFetching,
    mediaData:state.data,
    selectedType : state.selectedType,
    currentAddress : state.currentAddress,
    radius : state.radius
  }
}

class PlacesList extends Component {
    constructor(props){
      super(props);
      this.inputChanged = this.inputChanged.bind(this);
    }
    inputChanged(e){
      var store = this.props;
      var selectedType = this.props.selectedType;
      var radius = this.props.radius;
      if(e.target.value){
          var searchBox = new window.google.maps.places.SearchBox(document.getElementById('input-box'));
          searchBox.addListener('places_changed', function() {
              var places = searchBox.getPlaces();
              if(places.length>0){
                  var center = {lat:places[0].geometry.location.lat(), lng:places[0].geometry.location.lng()};
                  store.dispatch({type:'SET_ADDRESS', payload:places[0].formatted_address})
                  store.dispatch({type:'SET_CENTER_LOCATION',payload:center})
                  store.dispatch(fetchFromCenter(center, selectedType, radius));
              }
          });
      }
    }
    render(){
      if(this.props.loading){
        return(
          <Col xs={12} sm={12} className="top70 bottom70 text-center">
            <img className="imageAlignGiphy" src={giphy1} alt=""></img>
          </Col>
        )
      }else if(this.props.mediaData === "ZERO_RESULTS"){
        return(
          <Col xs={12} sm={12} className="bottom70 text-center">
            {(this.props && this.props.mediaData.length>0)?
              <FormControl className="top70 bottom50" type="text" placeholder="Search place" id="input-box" onChange={this.inputChanged} autoFocus/>:''
            }
            {(this.props && this.props.currentAddress)?<h4>{this.props.currentAddress}</h4>:''}
            <img className="imageAlign" src={empty_result} alt=""></img>
          </Col>
        )
      }else{
        return(
          <div>
            <Col xs={12} sm={6} className="bottom70">
              {(this.props && this.props.mediaData.length>0)?
                <FormControl className="top70 bottom50" type="text" placeholder="Search place" id="input-box" onChange={this.inputChanged} autoFocus/>:''
              }
              {(this.props && this.props.currentAddress)?<h4>{this.props.currentAddress}</h4>:''}
              {this.props && this.props.mediaData && this.props.mediaData.map((result, index) => {
                return(
                  <Media align="top" key={index} className="card">
                    <Media.Body>
                      <Media.Heading>{result.name}</Media.Heading>
                      <p>
                        {result.vicinity}
                      </p> 
                      <p className="mb0 bold">
                        {result.rating?((result.rating)+'/5'):""}    
                      </p>                  
                      <span className="pull-right">{result.opening_hours && (result.opening_hours.open_now===true?<h4 className="mt0"><Label bsStyle="primary">Open now</Label></h4>:"" || result.opening_hours.open_now===false?<h4 className="mt0"><Label bsStyle="default">Closed now</Label></h4>:"")}</span>
                      
                    </Media.Body>
                  </Media>
                )
              })}
            </Col>
          </div>
        )
      }
      
    }
  }

  export default connect(mapStateToProps)(PlacesList);
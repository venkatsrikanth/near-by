import React, { Component } from 'react';
import './App.css';

import AppNavbarAllow from './components/appNavbarAllow';
import Map from './components/map';
import PlacesList from './components/placesList';

import { Grid, Row } from 'react-bootstrap';
import { connect } from 'react-redux';

const mapStateToProps = (state)=>{
  return{
    access : state.userLocationAccess
  }
}

class App extends Component {
  render() {
    return (
      <div>
        <AppNavbarAllow></AppNavbarAllow>
        <Grid>
          <Row className="show-grid">
            <PlacesList></PlacesList>
            <Map></Map>
          </Row>
        </Grid>
      </div>
    ); 
  }
}

export default connect(mapStateToProps)(App);





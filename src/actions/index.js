


export function fetchResults(selectedType, currentLocation, radius) {
    return function(dispatch) {
        dispatch({type:'CHANGE_SELECTED_TYPE', payload:selectedType});
        return new Promise(function (resolve, reject) {
            if(currentLocation){
                dispatch({type:'FETCH_RESULTS_START'});
                dispatch(fetchFromCenter(currentLocation, selectedType));
            }else{
                navigator.geolocation.getCurrentPosition((latlngObj) => {
                    new window.google.maps.Geocoder().geocode({
                        'location': {lat:latlngObj.coords.latitude, lng: latlngObj.coords.longitude}
                     }, function(results, status) {
                        if (status === window.google.maps.GeocoderStatus.OK) {
                            dispatch({type:'SET_ADDRESS', payload:results[0].formatted_address})
                        } 
                     });
                    dispatch({type:'FETCH_RESULTS_START'});
                    var center = { lat: latlngObj.coords.latitude, lng: latlngObj.coords.longitude };
                    dispatch({type:'LOCATION_ACCESS', payload:true});
                    dispatch(fetchFromCenter(center, selectedType, radius));
                }, (block) =>{
                    if(block && block.message==="User denied Geolocation"){
                        dispatch({type:'LOCATION_ACCESS', payload:false})
                    }
                });
            }
              
        });        
    }
}

export function fetchFromCenter(center, selectedType, radius){
    return function(dispatch) {
        dispatch({type:'SET_CURRENT_LOCATION', payload:center});
        var service = new window.google.maps.places.PlacesService(document.createElement('div'));
        service.nearbySearch(
            {   location: center, 
                radius: (radius * 1000) || 1000, 
                types: [selectedType.type]
            }, function (results, status) {
                if (status === window.google.maps.places.PlacesServiceStatus.OK) {
                    dispatch({type:'FETCH_RESULTS_SUCCESS', payload:results});
                }else if(status === "ZERO_RESULTS"){
                    dispatch({type:'FETCH_RESULTS_SUCCESS', payload:"ZERO_RESULTS"});
                } else {
                    dispatch({type:'FETCH_RESULTS_ERR', payload:status});
                }
            }
        );
    }            
}
const initState = {
    data: [],
    types : [ 
        {name:"Bank's", type:"bank"},
        {name:"Atm's", type:"atm"},
        {name:"Pharmacy's", type:"pharmacy"},
        {name:"Hospital's", type:"hospital"},
        {name:"Airport's", type:"airport"},
        {name:"Local govt offices", type:"local_government_office"},
        {name:"Train stations", type:"train_station"},
        {name:"Bus stops", type:"bus_station"}
    ],
    isFetching : false,
    error:null,
    selectedType : {name:'Banks', type:'bank'},
    currentLocation : null,
    currentAddress : null,
    userLocationAccess : null,
    radius : 1
}

export default function(state = initState, action){
    switch(action.type){
        case "FETCH_RESULTS_START":{
            return {...state, isFetching:true}
            break;
        }
        case "FETCH_RESULTS_ERR":{
            return {...state, error:action.payload, isFetching:false}
            break;
        } 
        case "FETCH_RESULTS_SUCCESS":{
            return {...state, data:action.payload, isFetching:false}
            break;
        }
        case "CHANGE_SELECTED_TYPE":{
            return {...state, selectedType:action.payload}
            break;
        }
        case "SET_CURRENT_LOCATION":{
            return {...state, currentLocation:action.payload}
            break;
        }
        case "LOCATION_ACCESS":{
            return {...state, userLocationAccess:action.payload}
            break;
        }
        case "SET_CENTER_LOCATION":{
            return {...state, currentLocation:action.payload}
            break;
        }
        case "IS_FETCHING":{
            return {...state, isFetching:action.payload}
            break;
        }
        case "SET_ADDRESS":{
            return {...state, currentAddress:action.payload}
            break;
        }
        default:
            return state;
            break;
    }
}